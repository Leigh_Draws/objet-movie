
// Création d'un objet "movie" avec ses propriétés

const movie = {
    title: "The Prestige",
    date: "2006",
    director: "Christopher Nolan",
    actors: ["Hugh Jackman", "Christian Bale", "Michael Caine"],
    favorite: true,
    rating: 4.3 ,
    duration: 0,

//  Création de la méthode pour convertir les heures et minutes en minutes seulement
// La fonction prend 2 paramètres, les heures et les minutes
// D'abord elle multiplie le chiffre heures par 60 pour le convertir en minutes, puis l'ajoute au reste
// this. fait référence à l'objet en cours "movie" et appelle la propriété "duration" pour la définir en fonction du résultat de la méthode

    setDuration: function (heures, minutes) {
        this.duration = (heures * 60) + minutes;
    }
}

// console.log pour vérifier que tout fonctionne

console.log(movie.title);
console.log(movie.director)
movie.setDuration(2, 15);
console.log(movie.duration)

